package com.senti.synonyms;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.*;

public class ThesaurusImplTest {

    private ThesaurusImpl thesaurus;

    @Before
    public void setup() {
        thesaurus = new ThesaurusImpl();
        thesaurus.addSynonym("fridge", "refrigerator");
        thesaurus.addSynonym("fridge", "icebox");
        thesaurus.addSynonym("icebox", "cooler");
    }

    @Test
    public void getAllSynonyms() {
        Collection<String> fridgeSynonyms = thesaurus.getAllSynonyms("fridge");
        Assert.assertTrue(fridgeSynonyms.contains("refrigerator"));
        Assert.assertTrue(fridgeSynonyms.contains("icebox") && fridgeSynonyms.contains("cooler"));
        Collection<String> refrigeratorSynonyms = thesaurus.getAllSynonyms("refrigerator");
        Assert.assertTrue(refrigeratorSynonyms.contains("icebox") && refrigeratorSynonyms.contains("fridge") && refrigeratorSynonyms.contains("cooler"));
    }

    @Test
    public void removeSynonym() {
        thesaurus.removeSynonym("fridge", "icebox");
        Collection<String> fridgeSynonyms = thesaurus.getAllSynonyms("fridge");
        Assert.assertFalse(fridgeSynonyms.contains("icebox"));
        Assert.assertFalse(fridgeSynonyms.contains("cooler"));
        Assert.assertTrue(fridgeSynonyms.contains("refrigerator"));
    }

    @Test
    public void deleteWord() {
        thesaurus.deleteWord("icebox");
        Collection<String> iceboxSynonyms = thesaurus.getAllSynonyms("icebox");
        Assert.assertTrue(iceboxSynonyms.isEmpty());
        Collection<String> fridgeSynonyms = thesaurus.getAllSynonyms("fridge");
        Assert.assertTrue(fridgeSynonyms.contains("refrigerator") && !fridgeSynonyms.contains("icebox"));
    }
}