package com.senti.synonyms;

import java.util.Collection;

public interface Thesaurus {

    /*
        Add a pair of synonyms.
        Synonyms are both commutative and transitive. I.e. if a is a synonym of b, b is a synonym of a. If c is a synonym of b,
        c is a synonym of a.
     */
    void addSynonym(String word, String synonym);

    /*
        Remove pair of synonyms
     */
    void removeSynonym(String word, String synonym);

    /*
        Remove word from Thesaurus
     */
    void deleteWord(String word);

    /*
        Get all synonyms for a word
     */
    Collection<String> getAllSynonyms(String word);

}
